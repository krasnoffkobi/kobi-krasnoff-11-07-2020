"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var mysql = require('mysql');
var bodyParser = require("body-parser");
// Create a new express app instance
var app = express();
// parse requests of content-type: application/json
app.use(bodyParser.json());
var con = mysql.createConnection({
    host: "localhost",
    user: "kobi",
    password: "12345678",
    database: "propit_schema"
});
con.connect(function (err) {
    if (err)
        throw err;
    console.log("Connected!");
});
app.get('/', function (req, res) {
    res.send('Hello World!');
});
app.get('/gettaskslist', function (req, res, next) {
    con.query("SELECT * FROM tasks_list", function (err, result, fields) {
        if (err)
            throw err;
        res.json(result);
    });
});
app.put('/updatetesk/:id', function (req, res, next) {
    // Validate Request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }
    var sql = "UPDATE tasks_list SET username = ?, phone = ?, email = ?, createdate = ? WHERE idTasks_List = ?";
    con.query(sql, [req.body.username, req.body.phone, req.body.email, req.body.createdate, req.params.id], function (err, res2) {
        if (err) {
            console.log(err);
            res2.send(err);
            return;
        }
        if (res2.affectedRows == 0) {
            // not found Customer with the id
            res2.send('no affected rows');
            return;
        }
        // res.send(err);
    });
    res.send('ok');
});
app.post('/inserttask', function (req, res, next) {
    // Validate Request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }
    var sql = "INSERT INTO tasks_list (username, phone, email, createdate) VALUES (?, ?, ?, ?)";
    con.query(sql, [req.body.username, req.body.phone, req.body.email, req.body.createdate], function (err, res2) {
        if (err) {
            console.log(err);
            res2.send(err);
            return;
        }
        // res.send(err);
    });
    res.send('ok');
});
app.delete('/deletetask/:id', function (req, res, next) {
    var sql = "DELETE FROM tasks_list WHERE idTasks_List = ?";
    con.query(sql, [req.params.id], function (err, res2) {
        if (err) {
            console.log(err);
            res2.send(err);
            return;
        }
        if (res2.affectedRows == 0) {
            // not found Customer with the id
            res2.send('no affected rows');
            return;
        }
        // res.send(err);
    });
    res.send('ok');
});
app.listen(3000, function () {
    console.log('App is listening on port 3000!');
});
