import express = require('express');
const mysql = require('mysql');
const bodyParser = require("body-parser");

// Create a new express app instance
const app: express.Application = express();

// parse requests of content-type: application/json
app.use(bodyParser.json());

const con = mysql.createConnection({
    host: "localhost",
    user: "kobi",
    password: "12345678",
    database: "propit_schema"
});

con.connect(function(err: any) {
    if (err) throw err;
    console.log("Connected!");
});

app.get('/', function (req, res) {
    res.send('Hello World!');
});

app.get('/gettaskslist', (req, res, next) => {
    con.query("SELECT * FROM tasks_list", function (err: any, result: any, fields: any) {
        if (err) throw err;
        res.json(result);
    });
});

app.put('/updatetesk/:id', (req: any, res, next)  => {
    // Validate Request
    if (!req.body) {
        res.status(400).send({
        message: "Content can not be empty!"
        });
    }

    const sql = "UPDATE tasks_list SET username = ?, phone = ?, email = ?, createdate = ? WHERE idTasks_List = ?";

    con.query(
        sql,
        [req.body.username, req.body.phone, req.body.email, req.body.createdate, req.params.id],
        (err: any, res2: any) => {
          if (err) {
            console.log(err);
            res2.send(err);
            return;
          }
    
          if (res2.affectedRows == 0) {
            // not found Customer with the id
            res2.send('no affected rows');
            return;
          }
    
          // res.send(err);
        }
      );

    res.send('ok');
})

app.post('/inserttask', (req: any, res, next)  => {
    // Validate Request
    if (!req.body) {
        res.status(400).send({
        message: "Content can not be empty!"
        });
    }

    const sql = "INSERT INTO tasks_list (username, phone, email, createdate) VALUES (?, ?, ?, ?)";

    con.query(
        sql,
        [req.body.username, req.body.phone, req.body.email, req.body.createdate],
        (err: any, res2: any) => {
          if (err) {
            console.log(err);
            res2.send(err);
            return;
          }
    
          // res.send(err);
        }
      );

    res.send('ok');
})

app.delete('/deletetask/:id', (req: any, res, next)  => {
    const sql = "DELETE FROM tasks_list WHERE idTasks_List = ?";

    con.query(
        sql,
        [req.params.id],
        (err: any, res2: any) => {
          if (err) {
            console.log(err);
            res2.send(err);
            return;
          }
    
          if (res2.affectedRows == 0) {
            // not found Customer with the id
            res2.send('no affected rows');
            return;
          }
    
          // res.send(err);
        }
      );

    res.send('ok');
})

app.listen(3000, function () {
    console.log('App is listening on port 3000!');
});